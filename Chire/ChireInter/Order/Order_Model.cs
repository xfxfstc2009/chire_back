﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Class;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Order
{
    public class Order_Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public string student_id { get; set; }
        public Student_Model student { get; set; }
        public string class_id { get; set; }
        public Class_Model classModel { get; set; }
        public string order_type { get; set; }
        public double order_time { get; set; }
        public string detail { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string status { get; set; }
    }

    public class Order_List_Model
    {
        public List<Order_Model> willStartList { get; set; }
        public List<Order_Model> endList { get; set; }
        public List<Order_Model> list { get; set; }
    }

    public class Out_Order_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Order_List_Model data { get; set; }
    }
}