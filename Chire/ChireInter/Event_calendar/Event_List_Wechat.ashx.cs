﻿using Chire.ChireInter.Parent;
using Chire.ChireInter.Students;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Event_calendar
{
    /// <summary>
    /// Event_List_Wechat 的摘要说明
    /// </summary>
    public class Event_List_Wechat : IHttpHandler
    {
        HttpContext contextWithBase;
        string code = "001ZSWpk0pPSop1OPhok0zuUpk0ZSWp2";                       // 网页登陆的登陆信息

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            // 如果有微信code 传入
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
       
            getEventList();
        }
        #endregion

        #region 获取数据
        public void getEventList()
        {
            
            Event_Action eventAction = new Event_Action();
             List<Event_Wechat_Model> list = new List<Event_Wechat_Model>();
            if (code.Length > 0)
            {
                // 根据code 获取openId 
                Wechat_Action wechatAction = new Wechat_Action();
                string openId = wechatAction.getOpenIdWithCode(code);
                string nOpenId = "";

                // 1.根据openId 判断学生信息 是否为学生
               Student_Action studentAction = new Student_Action();
               Student_Model studentModel = studentAction.getStudentInfoWithId(openId);
               if (studentModel.id != null)
               {        // 表示是学生
                   nOpenId = openId;
               }
               else {
                   Parent_Action parentAction = new Parent_Action();
                   Student_Model parentStudentModel = parentAction.getStudentIdWithBindingParentId(openId);
                   nOpenId = parentStudentModel.id;
               }


                // 根据openId 进行筛选学生
               List<string> eventIdList = eventAction.getEventListWithUserId(nOpenId);
                for (int i = 0 ; i < eventIdList.Count;i++){
                    string eventId = eventIdList[i];
                    Event_Model eventModel = eventAction.getEventWithEventId(eventId);


                    // time 转换成date
                    DateTime dateTime = Convert.ToDateTime(eventModel.date);
                    DateTime timeDateTime = Convert.ToDateTime(eventModel.time);
                    string nTitle = timeDateTime.Hour.ToString() + ":" + timeDateTime.Minute.ToString() + "  " + eventModel.title;


                    string nTimeStr = dateTime.Day.ToString() + "-" + dateTime.Month.ToString() + "-" + dateTime.Year.ToString();
                    Event_Wechat_Model wechatEventModel = new Event_Wechat_Model() { title = nTitle, time =eventModel.time, date = nTimeStr };
                    list.Add(wechatEventModel);
                }
            }
            else { 
                list = eventAction.getWechatEventList();
            }

            successManager(list);
        }
        #endregion

        #region 成功方法
        public void successManager(List<Event_Wechat_Model> list)
        {
            OUTEvent_Wechat_List_Model out_base_setting = new OUTEvent_Wechat_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Event_Wechat_List_Model eventList = new Event_Wechat_List_Model();
            eventList.list = list;
            out_base_setting.data = eventList;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}