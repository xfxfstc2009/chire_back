﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Chire.ChireInter.Work
{
    /// <summary>
    /// Work_List 的摘要说明
    /// </summary>
    public class Work_List : IHttpHandler
    {
        HttpContext contextWithBase;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            getWorkList();
        }
        #endregion

        #region 获取作业列表
        public void getWorkList() { 
            Work_Action workAction = new Work_Action();
            List<Work_Model> workList = workAction.getWorkList();
            successManager(workList);
        }
        #endregion

        #region 执行方法
        public void successManager(List<Work_Model>workList)
        {
            OutWork_List_Model out_base_setting = new OutWork_List_Model();
          
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            Work_List_Model workListModel = new Work_List_Model();
            workListModel.list = workList;
            out_base_setting.data = workListModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
       
    }
}