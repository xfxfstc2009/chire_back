﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Subject
{
    /// <summary>
    /// Subject_del 的摘要说明
    /// </summary>
    public class Subject_del : IHttpHandler
    {
        HttpContext contextWithBase;
        string id = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("id"))
            {
                id = contextWithBase.Request.Form["id"];
            }
            deleteSubjectManager();
        }
        #endregion

        #region 查询学生表
        public void deleteSubjectManager()
        {
            Subject_Action subjectAction = new Subject_Action();
            subjectAction.deleteSubjectWithId(id);
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            OUTSubject_List_Model out_base_setting = new OUTSubject_List_Model();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}