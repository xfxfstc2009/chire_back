﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Parent;
using Chire.ChireInter.Students;
using Chire.ChireInter.Wechat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Chire.ChireInter.Identity
{
    /// <summary>
    /// 家长绑定学生
    /// </summary>
    public class Identity_Auth : IHttpHandler
    {
        HttpContext contextWithBase;
        string auth_type = "1";                   // 认证类型
        string code = "011cIyyW16VDf011XZxW18oeyW1cIyy-";                       // 传入code
        string authCode = "6415";                   // 认证码
        string studentId = "osQG9w5A_vMTgfevLVFAZpoQlEyY";

                  
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("auth_type"))
            {
                auth_type = contextWithBase.Request.Form["auth_type"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("code"))
            {
                code = contextWithBase.Request.Form["code"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("authCode"))
            {
                authCode = contextWithBase.Request.Form["authCode"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("studentId"))
            {
                studentId = contextWithBase.Request.Form["studentId"];
            }
            

            mainManager();
        }
        #endregion

        #region 其他
        public void mainManager() { 
            // 1. 根据code 获取openid
            Wechat_Action wechatAction = new Wechat_Action();
            string openId = wechatAction.getOpenIdWithCode(code);
            // 2.获取当前的认证状态
            Identity_Action identityAction = new Identity_Action();
            int authTypeInt = Convert.ToInt32(auth_type);
            Chire.ChireInter.Identity.Identity_Action.AuthType authType = (Chire.ChireInter.Identity.Identity_Action.AuthType)Enum.ToObject(typeof(Chire.ChireInter.Identity.Identity_Action.AuthType), authTypeInt);
            Identity_Model identityModel = identityAction.getAuthType(openId, authType);
            // 3. 获取认证状态
            if (identityModel.is_auth == 1) // 表示已认证
            {
                successManager("200", "认证成功", identityModel);
            }
            else
            {                               // 表示未认证
                // 1. 判断验证码是否正确
                if (identityModel.code.Equals(authCode))
                {    // 1.修改认证状态-认证成功
                     identityAction.authBindingSuccess(openId, auth_type);
                     // 2. 如果是家长进行绑定，就插入家长信息
                     if (auth_type.Equals("1") == true) {
                         Student_Action studentAction = new Student_Action();
                         Student_Model studentModel = studentAction.getStudentInfoWithId(studentId);
                         // 家长插入
                         Parent_Action parentAction = new Parent_Action();
                         parentAction.authBindingParentInfo(openId, studentModel.name, studentModel.id);
                         identityModel.student = studentModel;
                         // 告诉学生已经绑定成功
                         identityAction.WechatParentBindingSuccessPushManager(openId, studentModel.name);
                     }
                    // 3. 抛出认证成功
                    successManager("200","认证成功",identityModel);
                }
                else {          // 认证失败
                    successManager("-1", "认证失败，请查看输入的邀请码是否正确", identityModel);
                }
            }
        }
        #endregion

        #region 执行方法
        public void successManager(string errcode,string err, Identity_Model identyModel)
        {
            OUTIdentity_Model out_base_setting = new OUTIdentity_Model();
            out_base_setting.errCode = errcode;
            out_base_setting.errMsg = err;

            out_base_setting.data = identyModel;

            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
   
    }
}