﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chire.ChireInter.Activity
{
    public class Activity_Model
    {
        public string id { get; set; }
        public string title { get; set; }
        public string des { get; set; }
        public string datetime { get; set; }
        public long timeInter { get; set; }
        public List<string> imgList { get; set; } 
    }

    public class Activity_List_Model
    {
        public List<Activity_Model> list;
    }

       public class OUTActivity_List_Model
    {
        // 1. 错误码
        public string errCode { get; set; }
        // 2. 错误信息
        public string errMsg { get; set; }
        // 3. 返回数据内容
        public Activity_List_Model data { get; set; }
    }
       public class OUTActivity_Model
       {
           // 1. 错误码
           public string errCode { get; set; }
           // 2. 错误信息
           public string errMsg { get; set; }
           // 3. 返回数据内容
           public Activity_Model data { get; set; }
       }
}