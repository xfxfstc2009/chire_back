﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Chire.ChireInter.Download;
using Newtonsoft.Json;
using Chire.wechat;
using Chire.ChireInter.Students;

namespace Chire.ChireInter.Exam
{
    /// <summary>
    /// Exam_File_Add 的摘要说明
    /// </summary>
    public class Exam_File_Add : IHttpHandler
    {
        HttpContext contextWithBase;
        string name = "炽热教育营业执照-1";
        string url = "File/炽热教育营业执照-1.pdf";
        string fileId = "";

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            contextWithBase = context;
            analysisInfo();
        }

        #region 验证信息
        public void analysisInfo()
        {
            if (contextWithBase.Request.Form.AllKeys.Contains("name"))
            {
                name = contextWithBase.Request.Form["name"];
            }
            if (contextWithBase.Request.Form.AllKeys.Contains("url"))
            {
                url = contextWithBase.Request.Form["url"];
            }
            fileId = Constance.Instance.getRandomStr(18);
            insertIntoDiary();
        }
        #endregion

        #region 插入数据
        public void insertIntoDiary()
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam_file_list(name,url,datetime,file_id) values(@name,@url,@datetime,@file_id)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
            cmd.Parameters["@name"].Value = name;
            // 1. 日记标题
            cmd.Parameters.Add("@url", SqlDbType.VarChar, 5000);
            cmd.Parameters["@url"].Value = url;
            // 2.日记详情
            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = DateTime.Now.ToString();

            cmd.Parameters.Add("@file_id", SqlDbType.VarChar, 1000);
            cmd.Parameters["@file_id"].Value = fileId;
            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
            successManager();
        }
        #endregion

        #region 成功方法
        public void successManager()
        {
            // 插入试卷
            getAllStudent();

            OutDownLoad_List out_base_setting = new OutDownLoad_List();
            out_base_setting.errCode = "200";
            out_base_setting.errMsg = "接口请求成功";
            out_base_setting.data = null;


            JsonSerializer serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(new JsonTextWriter(sw), out_base_setting);
            string info = sw.GetStringBuilder().ToString();
            contextWithBase.Response.Write(sw.GetStringBuilder().ToString());
        }
        #endregion

        #region 给所有的学生插入这个卷子为0分
        public void getAllStudent(){
            Student_Action studentAction = new Student_Action();
            List<Student_Model> studentList = studentAction.getAllStudent("1","");

            for (int i = 0; i < studentList.Count; i++) {
                string linkId = Constance.Instance.getRandomStr(10) + i;

                Student_Model studentModel = studentList[i];
                insertExamInfo(studentModel.id, fileId, linkId);
            }
        }
        #endregion

        

        #region 插入试卷内容
        public void insertExamInfo(string studentId, string examId,string linkId)
        {
            // 连接数据库
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();
            string StrInsert = "";
            StrInsert = "insert into exam(student_id,exam_id,score,linkId,datetime) values(@student_id,@exam_id,@score,@linkId,@datetime)";
            SqlCommand cmd = new SqlCommand(StrInsert, sqlcon);
            // 添加参数并且设置参数值
            // 1. 作者id
            cmd.Parameters.Add("@student_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@student_id"].Value = studentId;
            // 1. 日记标题
            cmd.Parameters.Add("@exam_id", SqlDbType.VarChar, 50);
            cmd.Parameters["@exam_id"].Value = examId;
            // 1. 日记标题
            cmd.Parameters.Add("@score", SqlDbType.VarChar, 50);
            cmd.Parameters["@score"].Value = 0;
            // 1. 日记标题
            cmd.Parameters.Add("@linkId", SqlDbType.VarChar, 1000);
            cmd.Parameters["@linkId"].Value = linkId;
            // 2.日记详情
            cmd.Parameters.Add("@datetime", SqlDbType.VarChar, 1000);
            cmd.Parameters["@datetime"].Value = "";

            // 执行插入数据的操作
            cmd.ExecuteNonQuery();
            sqlcon.Close();
        }
        #endregion

        #region 其他
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}