﻿using Chire.ChireInter.Students;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace Chire.ChireInter.Exam
{
    public class Exam_Action
    {
        #region 获取平均分数
        public double getScoreAvg() {
            return 70.3;
         
        }	 
        
        #endregion

        #region 获取当前测验的结果
        public List<Exam_AllStudent_Single_Model> getAllExamDetailWithFileId(string fileId)
        {
            Student_Action studentAction = new Student_Action();
            
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam where exam_id = '"+ fileId +"'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<Exam_AllStudent_Single_Model> exam_list = new List<Exam_AllStudent_Single_Model>();
            for (int i = 0; i < rows; i++)
            {
                string id = dt.Rows[i]["id"].ToString();
                string student_id = dt.Rows[i]["student_id"].ToString();
                Student_Model studentModel = studentAction.getStudentInfoWithId(student_id);
                string exam_id = dt.Rows[i]["exam_id"].ToString();
                string score = dt.Rows[i]["score"].ToString();
                string marks = dt.Rows[i]["marks"].ToString();
                string datetime = dt.Rows[i]["datetime"].ToString();
                string linkId = dt.Rows[i]["linkId"].ToString();
                List<string> imgList = getExamsImgList(linkId);
            
                Exam_AllStudent_Single_Model examModel = new Exam_AllStudent_Single_Model()
                {
                   id = id,
                   student = studentModel,
                   score = score,
                   exam_id = exam_id,
                   marks = marks,
                   datetime = datetime,
                   linkId = linkId, 
                   imgList = imgList
                };
                exam_list.Add(examModel);
            }
            sqlcon.Close();
            return exam_list;
        }
        #endregion

        #region 获取当前测验结果的图片
        public List<string> getExamsImgList(string examId) {
            SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            sqlcon.Open();

            string strselect = "select * from exam_sub where linkId = '" + examId + "'";
            //查询音乐信息
            DataTable dt = new DataTable();

            SqlDataAdapter dap = new SqlDataAdapter(strselect, sqlcon);
            dap.Fill(dt);
            int rows = dt.Rows.Count;

            List<string> examImgList = new List<string>();
            for (int i = 0; i < rows; i++)
            {
                string fileUrl = dt.Rows[i]["fileUrl"].ToString();
                examImgList.Add(fileUrl);
            }
            sqlcon.Close();
            return examImgList;
        }

        #endregion

    }
}