﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chire.ChireInter.Task;
using Chire.wechat;
using FluentScheduler;

namespace Chire
{
    public class MyJobRegistry : Registry
    {
        public  MyJobRegistry()
        {
            // 1.将下周的课表下发到事件中去

            Schedule<EventAddEvaluate>().ToRunEvery(1).Weeks().On(DayOfWeek.Monday).At(01, 08);
            // 2.如果今天开课就对今天开课的进行提醒
            Schedule<EveryDayEvaluate>().ToRunEvery(1).Days().At(7, 00);
            // 3.课前进行提醒
            Schedule<EveryClassOneHourEvaluate>().ToRunNow().AndEvery(10).Minutes();
            // 4.每天晚上进行推送
            Schedule<EveryEventEvaluate>().ToRunEvery(1).Days().At(22, 30);
       
        }
    }

    #region 每天早上7点发送
    internal class EveryDayEvaluate : IJob
    {
        public void Execute()
        {
            Task_EveryDay taskManager = new Task_EveryDay();
            taskManager.mainManager();
        }
    }
    #endregion

    #region 每天课前1小时发送
    internal class EveryClassOneHourEvaluate : IJob
    {
        public void Execute()
        {
            Task_EveryClass taskManager = new Task_EveryClass();
            taskManager.mainManager();
        }
    }
    #endregion

    #region 每天晚上１０点小时发送课表事件等
    internal class EveryEventEvaluate : IJob
    {
        public void Execute()
        {
            Task_EveryDayNight taskManager = new Task_EveryDayNight();
            taskManager.mainManager();
        }
    }
    #endregion

    #region 每周日晚上11点小时添加课表事件等
    internal class EventAddEvaluate : IJob
    {
        public void Execute()
        {
            Task_EveryEventWeekDay taskManager = new Task_EveryEventWeekDay();
            taskManager.mainManager();
        }
    }
    #endregion

    internal class UpdateEvaluate : IJob  //此处实现IJob接口
    {
        public void Execute()//这个方法是IJob接口的方法，必须实现。这个方法也是上面所示的</pre> //Schedule<UpdateEvaluate>()...的入口方法<br>  
        { //此处添加自己的代码段 
            

        }
    }
}